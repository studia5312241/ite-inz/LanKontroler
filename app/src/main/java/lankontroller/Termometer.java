package lankontroller;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 *
 */
public class Termometer {
	private lankontroller.HttpRequest httpRequest;
	
	public Termometer(HttpRequest httpRequest) {
		this.httpRequest = httpRequest;
	}
	
	public double getTemperature(int termometerId) throws IOException {
		double temperature;
		String response = null;
		
		response = httpRequest.getResponse("/xml/ix.xml");
		
		Pattern patt = Pattern.compile("(?<=\\<ds"+termometerId+"\\>)(.*?)(?=<\\/ds"+termometerId+"\\>)");
		Matcher matcher = patt.matcher(response);
		if (matcher.find()) {
		    temperature = Integer.parseInt(matcher.group())/10.0;
		} else {
			temperature = -273;
		}
		return temperature;
	}
}
